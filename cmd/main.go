package main

import (
	"gitlab.com/khusainnov/animal-chipization/internal/app"
	"go.uber.org/zap"
)

func main() {
	log, _ := zap.NewDevelopment()

	log.Info("Starting application")
	if err := app.Run(log); err != nil {
		log.Fatal(zap.Error(err).String)
	}
}
