DB_USER=$(PG_USER)
DB_NAME=$(Test_DB_Name)
DB_PASS=$(PG_PASSWORD)
DB_HOST=$(PG_HOST)
DB_PORT=$(PG_PORT)
DB_SSL=$(PG_SSL)
DB_PATH=postgres://$(DB_USER):$(DB_PASS)@$(DB_HOST):$(DB_PORT)/$(DB_NAME)?sslmode=$(DB_SSL)

MOCKS_DESTINATION=mocks

.PHONY: mocks
mock:
	# put the files with interfaces you'd like to mock in prerequisites
	# wildcards are allowed
	mocks: pkg/party/greeter.go pkg/party/visitor-lister.go
		@echo "Generating mocks..."
		@rm -rf $(MOCKS_DESTINATION)
		@for file in $^; do mockgen -source=$$file -destination=$(MOCKS_DESTINATION)/$$file; done

m-up:
	migrate -path ./schema -database $(DB_PATH) up
m-down:
	migrate -path ./schema -database $(DB_PATH) down

d-up:
	docker run --name=postgres -e POSTGRES_DB=$(DB_NAME) -e POSTGRES_PASSWORD=$(DB_PASS) -p $(DB_PORT):5432 -d --rm postgres
d-stop:
	docker stop $(DB_NAME)
d-exec:
	docker exec -it $(DB_NAME) /bin/bash

run:
	go run cmd/main.go
build:
	GOOS=linux GOARCH=amd64 go build -o animals cmd/main.go
go/test:
	go test ./... -count=1
go/generate:
	go generate ./...

migrate-create:
	migrate create -ext SQL -digits 6 -format unix -tz "Local" -dir schema $(MIGRATE_NAME)