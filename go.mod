module gitlab.com/khusainnov/animal-chipization

go 1.20

require (
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.5
	go.uber.org/zap v1.24.0
)

require (
	github.com/lib/pq v1.10.7 // indirect
	gitlab.com/khusainnov/driver v0.1.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
