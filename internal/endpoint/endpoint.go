package endpoint

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/khusainnov/animal-chipization/internal/service"
)

type Endpoint struct {
	services *service.Service
}

func NewEndpoint(services *service.Service) *Endpoint {
	return &Endpoint{services: services}
}

func (e *Endpoint) InitEndpoint() *mux.Router {
	r := mux.NewRouter()

	{
		r.HandleFunc("/registration", e.CreateUser).Methods(http.MethodPost)
		r.HandleFunc("/accounts/{accountId:[0-9]+}", e.GetUser).Methods(http.MethodGet)
		r.HandleFunc("/accounts/search", nil).Methods(http.MethodGet)
		r.HandleFunc("/accounts/{accountId:[0-9]+}}", e.UpdateUser).Methods(http.MethodPut)
		r.HandleFunc("/accounts/{accountId:[0-9]+}}", e.DeleteUser).Methods(http.MethodDelete)
	}

	return r
}
