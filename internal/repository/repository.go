package repository

import "github.com/jmoiron/sqlx"

type User interface {
}

type Repository struct {
	User
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		User: NewUserDB(db),
	}
}
