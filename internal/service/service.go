package service

import "gitlab.com/khusainnov/animal-chipization/internal/repository"

type User interface {
}

type Service struct {
	User
}

func NewService(repo *repository.Repository) *Service {
	return &Service{
		User: NewUserService(repo),
	}
}
