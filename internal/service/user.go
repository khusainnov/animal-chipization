package service

import "gitlab.com/khusainnov/animal-chipization/internal/repository"

type UserService struct {
	repo repository.User
}

func NewUserService(repo repository.User) *UserService {
	return &UserService{repo: repo}
}
