package app

import (
	"context"
	"crypto/tls"
	"net/http"
	"time"
)

const (
	timeTTL = time.Second * 15
)

type Server struct {
	httpServer *http.Server
}

func (s *Server) Run(port string, endpoint http.Handler, tls *tls.Config) error {
	s.httpServer = &http.Server{
		Addr:              ":" + port,
		Handler:           endpoint,
		ReadTimeout:       timeTTL,
		ReadHeaderTimeout: timeTTL,
		WriteTimeout:      timeTTL,
		IdleTimeout:       timeTTL,
		MaxHeaderBytes:    1 << 20,
		TLSNextProto:      nil,
		ConnState:         nil,
	}

	if tls != nil {
		s.httpServer.TLSConfig = tls
	}

	return s.httpServer.ListenAndServe()
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}
