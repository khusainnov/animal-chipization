package app

import (
	"os"

	"gitlab.com/khusainnov/animal-chipization/internal/endpoint"
	"gitlab.com/khusainnov/animal-chipization/internal/repository"
	"gitlab.com/khusainnov/animal-chipization/internal/service"
	"gitlab.com/khusainnov/driver/postgres"
	"go.uber.org/zap"
)

const (
	defaultPort = "8080"

	defaultDBHost     = "localhost"
	defaultDBPort     = "5432"
	defaultDBUser     = "postgres"
	defaultDBPassword = "qwerty"
	defaultDBName     = "postgres"
	defaultSSL        = "disable"
)

func Run(log *zap.Logger) error {
	log.Info("connecting to db")
	db, err := postgres.NewPostgresDB(postgres.ConfigPG{
		Host:     getEnv("PG_HOST", defaultDBHost),
		Port:     getEnv("PG_PORT", defaultDBPort),
		User:     getEnv("PG_USER", defaultDBUser),
		Password: getEnv("PG_PASSWORD", defaultDBPassword),
		DBName:   getEnv("Test_DB_Name", defaultDBName),
		SSLMode:  getEnv("PG_SSL", defaultSSL),
	})
	if err != nil {
		log.Error("cannot connect to db", zap.Error(err))
		return err
	}
	defer func() {
		err = db.Close()
		if err != nil {
			log.Fatal("cannot close connection to db", zap.Error(err))
		}
	}()

	log.Info("initializing layers")
	repo := repository.NewRepository(db)
	services := service.NewService(repo)

	endpoints := endpoint.NewEndpoint(services)

	server := new(Server)

	log.Info("Running the server", zap.String("PORT", getEnv("PORT", defaultPort)))
	if err = server.Run(getEnv("HTTP_PORT", defaultPort), endpoints.InitEndpoint(), nil); err != nil {
		log.Error("cannot start the server", zap.Error(err))
		return err
	}

	return nil
}

func getEnv(name, defaultValue string) string {
	result, ok := os.LookupEnv(name)
	if ok {
		return result
	}

	return defaultValue
}
